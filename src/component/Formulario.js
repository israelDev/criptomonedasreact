import React , {useEffect, useState} from 'react';
import styled from '@emotion/styled'
import useMoneda from '../hooks/useMoneda'
import useCriptomonedas from '../hooks/useCriptomonedas'
import Error from './Error'
import axios from 'axios'
import PropTypes from 'prop-types';


const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width:100%;
    border-radius: 10px;
    color:#FFF;
    transition: background-color .3s ease;

    &:hover {
        background-color: #326AC0;
        cursor: pointer;
    }
`;


const Formulario = ({guardarCriptomoneda , guardarMoneda}) => {


    // state de las criptomonedas 

    const[listaCripto, guardarCriptomonedas] = useState([])
    const[error, guardarError] = useState(false)

    const MONEDAS = [
        { codigo : 'USD', nombre: 'Dolar de Estados Unidos'},
        { codigo : 'MXN', nombre: 'Peso Mexicano'},
        { codigo : 'EUR', nombre: 'Euro'},
        { codigo : 'GDP', nombre: 'Libra Esterlina'},
        { codigo : 'CLP', nombre: 'Peso Chileno'}
    ]

    // utilizar useMoneda
    const [moneda , SelectMonedas] = useMoneda('Elige tu Moneda', '',MONEDAS);

    // utilizar criptomonedas
    const [criptomoneda , SelectCripto] = useCriptomonedas('Elige tu Criptomoneda', '',listaCripto);

    //llamado de la API 

    useEffect(()=>{
        const consultarAPI = async () =>{
            const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD'
            const resultado = await axios.get(url);
            if(resultado.status === 200){
                guardarCriptomonedas(resultado.data.Data)
            }
        }

        consultarAPI();
    },[])

    const cotizarMoneda = e =>{
        e.preventDefault();

        if(moneda === '' || criptomoneda === ''){
            guardarError(true)
            return
        }

        guardarError(false)
        guardarCriptomoneda(criptomoneda)
        guardarMoneda(moneda)
    }
    return ( 
        <form
            onSubmit = {cotizarMoneda}
        >
            {error?<Error mensaje='Todos los campos son obligatorios'/>:null}
            <SelectMonedas/>
            <SelectCripto/>

            <Boton 
                type="submit"
                value= "calcular"
            />
        </form>

     );
}

Formulario.propTypes = {
    guardarCriptomoneda : PropTypes.func.isRequired,
    guardarMoneda : PropTypes.func.isRequired
}
 
export default Formulario;