import React from 'react';
import styled from '@emotion/styled'
import PropTypes from 'prop-types';

const MensajeError = styled.p`
    background-color: #b7322c;
    padding: 1rem;
    color: #FFF;
    font-size: 30px;
    text-align: center;
    text-transform: uppercase;
    font-weight: bold;
    font-family: 'Bebas Neue', cursive;
`;

const Error = ({mensaje}) => {
    return ( 
        <MensajeError>{mensaje}</MensajeError>
     );
}

Error.propTypes = {
    mensaje: PropTypes.string.isRequired
}

export default Error;